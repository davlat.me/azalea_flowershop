let flowers = [
  {
    id: 1,
    title: "Антуриум",
    price: '250.000-700.000',
    img: "https://toshkentgullari.uz/thumb/2/B-6yrJ8q7-1mZW3tOVI4Jw/408r408/d/s1200_1_1.jpg",
  },
  {
    id: 2,
    title: "Спатифиллум",
    price: '250.000-800.000',
    img: "https://toshkentgullari.uz/thumb/2/rb3zZYAUAa4Anr4_u06buw/408r408/d/spacifilumm_0.jpg",
  },
  {
    id: 3,
    title: "Драцена",
    price: '50.000-2.500.000',
    img: "https://toshkentgullari.uz/thumb/2/N5zu9kNRrQ08WFkr-GoAHg/408r408/d/dracaena-fragrans-lemon-lime-1m.jpg",
  },
  {
    id: 4,
    title: "Диффенбахия",
    price: '200.000-1.000.000',
    img: "https://toshkentgullari.uz/thumb/2/8PNS39ciPxgt-1PwtTirjQ/408r408/d/209520-00-gpie.jpg",
  },
  {
    id: 5,
    title: "Каланхоэ",
    price: '70.000-200.000',
    img: "https://toshkentgullari.uz/thumb/2/W9xiG1pcke2wjb4NsuqvlA/408r408/d/2-45.jpg",
  },
  {
    id: 6,
    title: "Бегония",
    price: '100.000-250.000',
    img: "https://toshkentgullari.uz/thumb/2/AxOhLQDeOp5cMsOy4IUlBw/408r408/d/s1200_1.jpg",
  },
  {
    id: 7,
    title: "Азалия",
    price: '100.000-300.000',
    img: "https://toshkentgullari.uz/thumb/2/J0cVOqgWq9AvD1XoYLxw6g/408r408/d/2d7bfe9c7ee5afccda39f6d3b72813f4.jpg",
  },
  {
    id: 8,
    title: "Камелия",
    price: '250.000-400.000',
    img: "https://rastenievod.com/wp-content/uploads/2016/11/1-67-700x659.jpg",
  },
  {
    id: 9,
    title: "Пахира",
    price: '150.000-350.000',
    img: "https://rastenievod.com/wp-content/uploads/2016/12/1-43-700x700.jpg",
  },
  {
    id: 10,
    title: "Кактус",
    price: '10.000-200.000',
    img: "https://toshkentgullari.uz/thumb/2/ohVqFXl7l4YxMasJ9n4RAg/408r408/d/kaktus.jpg",
  },
  {
    id: 11,
    title: "Араукария",
    price: '100.000-2.000.000',
    img: "https://cdn.botanichka.ru/wp-content/uploads/2021/12/araukariya-komnatnaya-novogodnyaya-yolka-03.jpg",
  },
  {
    id: 12,
    title: "Лилия",
    price: '180.000-500.000',
    img: "https://prosad.ru/wp-content/uploads/loaded/495407efffa064c641ed122992.jpg",
  },
  {
    id: 13,
    title: "Магнолия",
    price: '500.000-20.000.000',
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScFiU6kA6QKFAeDkJiM1zNKn1WE8T23miUVw&usqp=CAU",
  },
  {
    id: 14,
    title: "Черный принц",
    price: '100.000-1.500.000',
    img: "https://kvartblog.ru/uploads/post_photo/6997/64706/post_9324.jpg",
  },
  {
    id: 15,
    title: "Шефлера",
    price: '40.000-2.500.000',
    img: "https://www.ocvetah.com/wp-content/uploads/2018/09/cvetok_Shefflera_6_24142029.jpg",
  }
];

let catalog_box = document.querySelector(".catalog-box");
let button_hide = document.querySelector(".hidden-btn")
let button_p = button_hide.querySelector('p')
let button_img = button_hide.querySelector('img')
button_p.innerHTML = 'Увидеть больше'

button_hide.onclick = () => {
  if(button_p.innerHTML === 'Увидеть больше'){
    button_p.innerHTML = 'Уменшить'
    catalog_box.classList.add('button_active')
    button_img.src = './img/up.png'
  }
  else{
    button_p.innerHTML = 'Увидеть больше'
    catalog_box.classList.remove('button_active')
    button_img.src = './img/down.png'
  }
}

function reload(name, box) {
  for (let item of name) {
    let flower_item = document.createElement("div");
    let flower_img = document.createElement("img");
    let flower_title = document.createElement("span");
    let flower_price = document.createElement("p");

    flower_item.classList.add("item");
    flower_img.src = item.img;
    flower_img.alt = item.title;
    flower_title.innerHTML = item.title;
    flower_price.innerHTML = item.price + ' ' + 'сум'

    flower_item.append(flower_img);
    flower_item.append(flower_title);
    flower_item.append(flower_price);
    box.append(flower_item);
  }
}
reload(flowers, catalog_box);
